<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/login.css')}}">
</head>
<body>
    <div class="header" align="center">
        <img src="{{URL::asset('/image/AppLogo.png')}}" id="logo" name="appLogo" ><br>
        <h4>Welcome to Super Mom Control Panel<br></h2>
        <h3>Log In to Your Account<br></h2>
    </div>
    <form  class="loginForm" align="center" class="login" method="POST" action="{{url('/login/checkLogin')}}">
        {{ csrf_field() }}
        @if(isset(Auth::user()->email))
            <script>
                window.location = "/login/successlogin";
            </script>
        @endif

        @if($message = \Session::get('error'))
            <div class="alert-block">
                <i class="material-icons" size="20" style="color:red">error</i>
                <strong>{{$message}}</strong></li>
            </div>
        @endif

        <div class="email">
            <div class="email{{ $errors->has('email') ? ' has-error' : '' }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}<br></strong>
                    </span>
                @endif
                <input size="30" id="email" name="username" value="{{old('email')}}" placeholder ="Username" autofocus><br>
            </div>
        </div>

        <div class="password">
            <div class="password{{ $errors->has('password') ? ' has-error' : '' }}">
               @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}<br></strong>
                    </span>
                @endif 
               <input size="30" type="password" id="password" name="password" value="{{old('password')}}" placeholder= " Password" autofocus><br>
            </div>
        </div>

        <div class="loginBtn">
            <button size="30" type="submit" class ="button"style="font-size:12px">Log In <i class="fa fa-sign-in"></i>
        </div>  

        <div class="forgetPwd">
            <a id="forget" href="{{url('/welcome/forgetPass')}}">Forget Your Password?</a> 
        </div>
    </form>
</body>
</html>