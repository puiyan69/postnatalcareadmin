<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{URL::asset('css/header.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/forgetPassword.css')}}">
</head>
<body>
    @include('layouts.app')
    <form class="forgetForm" >
        <div class="caption">
            <p id="subTitle"> Admin Reset Password </p>
        </div>
        <hr>
        <div class="email">
            <label>E-mail Address</label>
            <input size="30" type="email" value="{{ old('email') }}" autofocus><br>
        </div>
        <button type="submit" id="send">Send Password Reset Link</button>
    </form>
</body>
</html>