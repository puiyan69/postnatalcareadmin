<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{URL::asset('css/confinementMenu.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/confinementRecipeTable.css')}}">
</head>
<style>
    h3{
        text-align : center;
        color : red;
        font-family : Georgia;
    }
</style>
<body>
@include('Common.homePage')

<button onclick="window.location='{{route('confinement.addRecipe')}}'" id="add">Add Recipe</button>

<form  method="GET" class="searchRecipeClass" action="{{URL::route('confinement.searchRecipe')}}">
    <input type="text" placeholder="Search.." name="search">
    <button type="submit"><i class="fa fa-search"></i></button>
</form>

@if ($recipes->isEmpty())
    <h3>No recipe name with such keyword found...</h3>
@else
    @include('confinementAdmin.confinementRecipeTable')
@endif
</body>
</html>
