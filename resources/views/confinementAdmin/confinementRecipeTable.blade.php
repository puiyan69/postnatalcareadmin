<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="{{URL::asset('css/confinementRecipeTable.css')}}">
</head>
<body>
    <table id="recipes">
        <col width="40">
        <col width="150">
        <col width="350">
        <col width="350">

        <tr>
            <th>No.</th>
            <th> Recipe Name </th>
            <th> Ingredients </th>
            <th> Instructions </th>
            <th id="action"></th>
            <th></th>
        </tr>

        <!-- Table Body -->
        <?php $number =1; ?>
        @foreach ($recipes as $value)
        <tr>
            <td>{{$number}}.</td>
            <td>{{$value -> recipeName}}</td>
            <td>
                {!! str_limit(nl2br($value-> ingredients),200) !!}<br><br>
            </td>
            <td>
                {!! str_limit(nl2br($value -> steps),200) !!}<br><br>
            </td>
            <td>
                <a href="/confinementAdmin/editRecipe/{{$value->id}}">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <button  id="trashBtn">
                    <i class="far fa-trash-alt"></i>
                </button>
            <td>
        </tr>
        <?php $number++;?>
        @endforeach
    </table>

    <!-- The modal for prompt delete confirmation -->
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
            <i class="fas fa-exclamation-circle"  style="font-size:80px"></i></li>
            </div>
            <div class="modal-body">
                <h2>Are you sure?<br></h2>
                <p>You won't be able to revert this!</p>
            </div>
            <div class="modal-footer">
                <button onclick="window.location='/confinementAdmin/confinementMenu/{{$value->id}}'" id="confirmDeleteBtn"> Yes, delete it </button> 
                <button id="cancelDeleteBtn"> No, cancel it </button> 
            </div>
        </div>
    </div>
</body>
<script>
//show modal script
var modal = document.getElementById('myModal');
var btns = document.querySelectorAll('#trashBtn'); 
[].forEach.call(btns, function(el) {
  el.onclick = function() {
      modal.style.display = "block";
  }
})

//cancel delete script
var cancelDeleteBtn = document.getElementById("cancelDeleteBtn");
cancelDeleteBtn.onclick = function(){
    modal.style.display = "none";
}
</script>
</html>