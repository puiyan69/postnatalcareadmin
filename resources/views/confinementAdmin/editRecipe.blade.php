<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
<style>
body{
    margin-left : 290px;
    margin-right : 100px;
}
#label{
    padding : 20px 20px 20px 0px;
    font-weight : bold;
}

.recipe-edit-info{

}
.fa-pencil-alt{
    cursor : pointer;
    float : right;
}
.fa-pencil-alt:hover{
    color : grey;
}
#info{
    margin-bottom : 40px;
}
#update{
    float : right;
}
h2{
    margin-left : 0px;
    font-family : Lucida Bright;
    color : #606060;
}
.div-name input{
    width : 400px;
    padding : 5px 0px 5px 0px;
    border : none;
    background-color : #FEEFFF;
}
.div-ingredient textarea, .div-steps textarea{
    width: 720px;
    height: 250px;
    border : none;
    resize : none;
    background-color : #FEEFFF;
}
#update{
    padding : 5px 20px 5px 20px;
    font-weight : bold;
    font-size : 14px;
    background-color : #29B46E;
    margin-bottom : 10px;
}
</style>
</head>
<body>
@include('Common.homePage')

<h2>Edit Recipe</h2><br>
@foreach ($recipes as $value)
<form clas="editClass" action="/confinementAdmin/editRecipe/{{$value->id}}" method="POST">
{{ csrf_field() }}
        <div class="div-name">
            <label id="label">Recipe Name</label>
            <hr>
            <div id="info">
                <input name="recipeName" value="{{$value -> recipeName}}">
            </div>
        </div>

        <div class="div-ingredient">
            <label id="label">Ingredients</label>
            <hr>
            <div id="info">
                <textarea name="ingredients">{{$value -> ingredients}}</textarea>
            </div>
        </div>

        <div class="div-steps">
            <label id="label">Instructions</label>
            <hr>
            <div id="info">
                <textarea name = "steps">{{$value -> steps}}</textarea>
            </div>
        </div>
        
        <button id="update" type="submit">Update</button>
@endforeach
</form>
</body>
</html>