<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="{{URL::asset('css/addRecipe.css')}}">
</head>
<style>
    .alert-danger{
        background-color : rgb(255, 244, 193, 0.5);
        width : 100%;
        margin:0 auto;
        text-align : center;
        border-radius : 10px;
    }

    .fa-exclamation-circle{
       color : #FF6868;
       margin-right : 10px;
    }

    #errorTitle{
        color : red;
    }

</style>
<body>
@include('Common.homePage')
<form class="addRecipeForm" method="POST" action="{{URL::route('confinement.store')}}">
    {{ csrf_field() }}

   @if(count($errors))
			<div class="alert alert-danger">
				<p id="errorTitle"><strong>Whoops!   There were some problems with your input.</strong></p>
				<ul>
                    @foreach($errors->all() as $error)
                    
					<li><i class="fas fa-exclamation-circle"></i>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
    @endif
    
    <label>Recipe Name<br></label>
    <input id="name" name="recipeName" value="{{ old('recipeName') }}"><br>
    <hr>

    <label>Ingredients<br></label>
    <textarea id="ingredients" name="ingredients" col="100" row="100">{{ old('ingredients')}}</textarea><br>
    <hr>

    <label>Steps<br></label>
    <textarea id="steps" name="steps" col="100" row="100">{{ old('steps') }}</textarea><br>

    <button align="center" type="submit" id="uploadBtn">Confirm</button>
</form>
</body>
</html>
