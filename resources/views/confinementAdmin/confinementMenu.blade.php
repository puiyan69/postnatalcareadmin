<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{URL::asset('css/confinementMenu.css')}}">
</head>
<style>

</style>
<body>
    @include('Common.homePage')

    <button onclick="window.location='{{route('confinement.addRecipe')}}'" id="add">Add Recipe</button>
    
    <form  method="GET" class="searchRecipeClass" action="{{URL::route('confinement.searchRecipe')}}">
        <input type="text" placeholder="Search.." name="search">
        <button type="submit"><i class="fa fa-search"></i></button>
    </form>

    @include('confinementAdmin.confinementRecipeTable')
</html>