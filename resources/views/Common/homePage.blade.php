<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{URL::asset('css/homePage.css')}}">
<link href='https://fonts.googleapis.com/css?family=Acme' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <!-- <div class="vl"></div> -->
    <div class="sideBar">
        <img src="{{URL::asset('/image/AppLogo.png')}}" id="logo" name="appLogo" ><br>
        <div class="dropdownMenu">
            <button class="dropbtn" onclick="myFunction()">Content Management<i class="fa fa-angle-down"></i></button>
            <div class="dropdown-content" id="myDropdown">
                <button class="dropbtn1" onclick="careTipsFunction()">Care Tips<i class="fa fa-angle-down"></i></button>
                    <div class="dropdown-content1" id="careTipsDrop">
                        <li><a href="#">Mother Care</a></li>
                        <li><a href="#">Baby Care</a></li>
                    </div>
                <li><a href="#">Postpartum Depression</a></li>
                <li><a href="{{URL::route('confinement.home')}}">Confinement</a></li>
            </div>
        </div> 
        <li><a href="#">Forum Management</a></li>
        <li><a href="#">Setting</a></li>
    </div>
</body>
<script>
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    function careTipsFunction() {
        document.getElementById("careTipsDrop").classList.toggle("show");
    }

    window.onclick = function(e) {
        if (!e.target.matches('.dropbtn')) {
            var myDropdown = document.getElementById("myDropdown");
            if (myDropdown.classList.contains('show')) {
             myDropdown.classList.remove('show');
            }
        }
    }
    window.onclick = function(e) {
        if (!e.target.matches('.dropbtn1')) {
            var myDropdown = document.getElementById("careTipsDrop");
            if (myDropdown.classList.contains('show')) {
                myDropdown.classList.remove('show');
            }
        }
    }
</script>
</html>