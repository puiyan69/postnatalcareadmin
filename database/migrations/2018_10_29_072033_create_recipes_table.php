<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "recipes";

    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            //$table->String('file');
            $table->text('recipeName');
            $table->longText('ingredients');
            $table->longText('steps');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
