<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/Authentication/login');
});

Auth::routes();

Route::post('/login/checkLogin', 'Authentication\LoginController@checkLogin');
Route::get('/login/successlogin', 'Authentication\LoginController@successlogin');
Route::get('/login/logout', 'Authentication\LogoutController@logout');
Route::get('/login/forgetPass', 'Authentication\ForgotPasswordController@forgotPassword');

//login as confinement user and direct to confinement admin home page
Route::get('/Common/homePage', 'Authentication\LoginController@successlogin') -> name('Common.homePage');


//confinement home page
Route::get('/confinementAdmin/confinementMenu', 'Confinement\HomeController@confinementHome') -> name('confinement.home');

//addRecipe page
Route::get('/confinementAdmin/addRecipe', 'Confinement\HomeController@addRecipe') -> name('confinement.addRecipe');

//post recipe
Route::post('/confinementAdmin/addRecipe', 'Confinement\StoreRecipeController@store') -> name('confinement.store');

Route::post('/confinementAdmin/editRecipe/{id}', 'Confinement\UpdateRecipeController@updateRecipe')  -> name('confinement.updateRecipe');
Route::get('/confinementAdmin/editRecipe/{id}', 'Confinement\UpdateRecipeController@editRecipe')-> name('confinement.editRecipe');

Route::get('/confinementAdmin/confinementMenu/{id}', 'Confinement\DeleteRecipeController@deleteRecipe') -> name('confinement.deleteRecipe');

Route::get('/confinementAdmin/confinementSearchResult', 'Confinement\SearchRecipeController@search') -> name('confinement.searchRecipe');
