<?php

namespace App\Http\Controllers\Authentication;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;

class ForgotPasswordController extends Controller
{
    public function forgotPassword(){
        return view('/Authentication/forgetPass');
    }
}
