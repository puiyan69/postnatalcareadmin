<?php

namespace App\Http\Controllers\Authentication;
use DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    function checkLogin(Request $request){
        $this -> validate($request,[
            'username' => 'required',
            'password' => 'required|alphaNum|min:6'
        ]);

        $remember = $request -> get('remember');

        if(Auth::attempt(['username' =>$request->get('username'),'password' => $request -> get('password')], $remember)){
            return redirect('/Common/homePage');
        }else{
            return back()->with('error', 'Wrong Username or Password');
        }
    }

    function successlogin(){
        return view('/Common/homePage');
    }
}
