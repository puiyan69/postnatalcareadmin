<?php

namespace App\Http\Controllers\Confinement;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Recipe;

class SearchRecipeController extends Controller
{
    //
    public function search()
    {
        $search = \Request::get('search');

        $recipes = Recipe::where('recipeName', 'like', '%' .$search .'%') ->orderBy('recipeName') -> paginate(20);
   
        return view('confinementAdmin.confinementSearchResult', compact('recipes'));
    }

}
