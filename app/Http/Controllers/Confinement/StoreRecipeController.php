<?php

namespace App\Http\Controllers\Confinement;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Recipe;
use Illuminate\Support\Facades\Input;

class StoreRecipeController extends Controller
{
    function store(Request $request)
    {
        $this -> validate($request, [
            'recipeName' => 'required',
            'steps' => 'required',
            'ingredients' => 'required'
            
        ], [
            'recipeName.required' => 'The Recipe Name field is required.',
            'steps.required' => 'The Steps field is required.',
            'ingredients.required' => 'The Ingredients field is required.'
        ]);
        
        $recipes = new Recipe;
        $recipes->fill($request->all());
        $recipes->save();
        return redirect()->route('confinement.home');
    }
}
