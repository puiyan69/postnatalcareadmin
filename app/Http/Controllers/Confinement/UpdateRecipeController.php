<?php

namespace App\Http\Controllers\Confinement;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Recipe;
use Illuminate\Support\Facades\Input;

class UpdateRecipeController extends Controller
{
    function editRecipe($id){
        $recipes['recipes'] = Recipe::find($id);
        $recipes['recipes'] = DB::select('select * from recipes where id = ? ', [$id]);
        return view('/confinementAdmin/editRecipe', $recipes);
    }

    function updateRecipe(Request $request, $id){


        $recipes = Recipe::find($id);
        if(!$recipes) throw new ModelNotFoundException;
     
        $recipes->fill($request->all());

        $recipes ->save();

        return redirect() -> route('confinement.home');
    }
}
