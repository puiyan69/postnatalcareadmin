<?php

namespace App\Http\Controllers\Confinement;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Recipe;
use Illuminate\Support\Facades\Input;

class DeleteRecipeController extends Controller
{
    function deleteRecipe($id) {
        DB::delete('delete from recipes where id = ?',[$id]);
        return redirect() -> route('confinement.home');
     }
}
