<?php

namespace App\Http\Controllers\Confinement;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Recipe;
use Illuminate\Support\Facades\Input;

class homeController extends Controller
{
    function confinementHome(){
        $recipes = new Recipe;
       $recipes['recipes'] =  DB::select('select * from recipes');
        return view('confinementAdmin/confinementMenu', $recipes);
    }
    
    function addRecipe(){
        return view('confinementAdmin.addRecipe');
    }
}
 